FROM python:3.12.5-alpine
LABEL maintainer="afk@ellugar.co"

# The enviroment variable ensures that the python output is set straight
# to the terminal with out buffering it first
ENV PYTHONUNBUFFERED 1


RUN apk add --no-cache --virtual .build-deps \
        gcc \
        git \
        jpeg-dev \
        libc-dev \
        linux-headers \
        make \
        musl-dev \
        pcre-dev \
        postgresql-dev \
        zlib-dev \
        libffi-dev

RUN mkdir /web
WORKDIR /web

COPY ./src/requirements.txt.lock requirements.txt.lock
RUN pip install -r ./requirements.txt.lock

COPY ./src /web

RUN python manage.py collectstatic --noinput
RUN python manage.py graphql_schema

EXPOSE 8000

ENTRYPOINT ["gunicorn", "-b", ":8000", "apiArlefreak.wsgi:application"]
