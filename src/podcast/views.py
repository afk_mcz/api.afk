from rest_framework import permissions, viewsets

from .models import Episode, Podcast, TaggedPodcast
from .serializers import EpisodeSerializer, PodcastSerializer, PodcastTagSerializer


class PodcastViewSet(viewsets.ModelViewSet):
    queryset = Podcast.objects.all()
    serializer_class = PodcastSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class EpisodeViewSet(viewsets.ModelViewSet):
    queryset = Episode.objects.all()
    serializer_class = EpisodeSerializer
    filter_fields = ("publish", "podcast__slug")
    lookup_field = "slug"
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)


class PodcastTagViewSet(viewsets.ModelViewSet):
    queryset = TaggedPodcast.objects.all().distinct("tag")
    serializer_class = PodcastTagSerializer
    permission_classes = (permissions.IsAuthenticatedOrReadOnly,)
