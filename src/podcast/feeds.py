from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed, Rss201rev2Feed

from .models import Episode, Podcast

GOOGLE_SCHEMA_URL = "http://www.google.com/schemas/play-podcasts/1.0"
ITUNES_SCHEMA_URL = "http://www.itunes.com/dtds/podcast-1.0.dtd"


class ExtendedRSSFeed(Rss201rev2Feed):
    def rss_attributes(self):
        attrs = super(ExtendedRSSFeed, self).root_attributes()
        attrs["version"] = self._version
        attrs["xmlns:atom"] = "http://www.w3.org/2005/Atom"
        attrs["xmlns:content"] = "http://purl.org/rss/1.0/modules/content/"
        attrs["xmlns:itunes"] = ITUNES_SCHEMA_URL
        attrs["xmlns:googleplay"] = GOOGLE_SCHEMA_URL
        return attrs

    def add_root_elements(self, handler):
        super(ExtendedRSSFeed, self).add_root_elements(handler)
        handler.addQuickElement(u"itunes:subtitle", self.feed["subtitle"])
        handler.addQuickElement(u"itunes:author", self.feed["author_name"])
        handler.addQuickElement(u"itunes:summary", self.feed["description"])
        handler.addQuickElement(u"googleplay:description", self.feed["description"])
        handler.addQuickElement(u"itunes:explicit", self.feed["iTunes_explicit"])
        handler.startElement(u"itunes:owner", {})
        handler.addQuickElement(u"itunes:name", self.feed["author_name"])
        handler.addQuickElement(u"itunes:email", self.feed["iTunes_email"])
        handler.endElement(u"itunes:owner")
        handler.startElement(u"image", {})
        handler.addQuickElement(u"url", self.feed["image_url"])
        handler.addQuickElement(u"title", self.feed["title"])
        handler.addQuickElement(u"link", self.feed["link"])
        handler.endElement(u"image")
        handler.addQuickElement(u"itunes:image", None, {"href": self.feed["image_url"]})
        handler.startElement(u"itunes:category", {"text": self.feed["parent_category"]})
        handler.addQuickElement(
            u"itunes:category", None, {"text": self.feed["child_category"]}
        )
        handler.endElement(u"itunes:category")

    def add_item_elements(self, handler, item):
        super(ExtendedRSSFeed, self).add_item_elements(handler, item)
        handler.addQuickElement(u"content:encoded", item["content_encoded"])
        handler.addQuickElement(u"itunes:title", item["title"])
        handler.addQuickElement(u"itunes:duration", item["duration"])
        handler.addQuickElement(u"itunes:explicit", item["explicit"])
        handler.addQuickElement(u"itunes:author", item["author"])
        handler.addQuickElement(u"itunes:subtitle", item["subtitle"])
        handler.addQuickElement(u"itunes:summary", item["description"])
        handler.addQuickElement(u"googleplay:description", item["description"])
        handler.addQuickElement(u"itunes:season", item["season"])
        handler.addQuickElement(u"itunes:episode", item["episode"])
        handler.addQuickElement(u"itunes:episodeType", item["episode_type"])
        handler.addQuickElement(
            u"itunes:image", None, {"href": item["iTunes_image_url"]}
        )
        handler.addQuickElement(
            u"enclosure",
            None,
            {
                "url": item["audio_url"],
                "type": item["audio_type"],
                "length": item["audio_size"],
            },
        )


class PodcastFeed(Feed):
    feed_type = ExtendedRSSFeed
    iTunes_explicit = u"no"

    def feed_extra_kwargs(self, obj):
        extra_args = {
            "iTunes_email": obj.author_mail,
            "iTunes_explicit": u"no",
            "image_url": obj.image.url,
            # 'iTunes_keywords': str(obj.tags),
            "parent_category": obj.parent_category,
            "child_category": obj.child_category,
        }

        return extra_args

    def item_extra_kwargs(self, item):
        return {
            "iTunes_image_url": item.image.url,
            "author": item.podcast.author,
            "duration": str(item.duration),
            "explicit": u"no",
            "audio_url": item.tracked_mp3,
            "audio_type": item.audio_type,
            "audio_size": str(item.audio_size),
            "subtitle": item.small_text,
            "slug": item.slug,
            "season": str(item.season),
            "episode": str(item.episode_number),
            "episode_type": item.episode_type,
            "content_encoded": self.item_content_encoded(item),
        }

    def get_object(self, request, podcast_slug):
        return Podcast.objects.get(slug=podcast_slug)

    def title(self, obj):
        return obj.title

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return obj.plain_text()

    # TODO: Override settings language
    def language(self, obj):
        return obj.language

    def author_name(self, obj):
        return obj.author

    def items(self, obj):
        return Episode.objects.filter(podcast=obj, publish=True)

    def subtitle(self, obj):
        return obj.small_text

    def item_pubdate(self, item):
        return item.dateCreated

    def item_updateddate(self, item):
        return item.dateUpdated

    def item_description(self, item):
        return item.small_text

    def item_title(self, item):
        return item.title

    def item_content_encoded(self, item):
        return item.encoded_text()


class AtomPodcastFeed(PodcastFeed):
    feed_type = Atom1Feed
    subtitle = PodcastFeed.description
