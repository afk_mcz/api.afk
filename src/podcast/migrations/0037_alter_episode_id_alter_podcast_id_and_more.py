# Generated by Django 5.1.2 on 2024-10-09 00:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("podcast", "0036_alter_taggedpodcast_tag"),
    ]

    operations = [
        migrations.AlterField(
            model_name="episode",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
        migrations.AlterField(
            model_name="podcast",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
        migrations.AlterField(
            model_name="taggedpodcast",
            name="id",
            field=models.BigAutoField(
                auto_created=True, primary_key=True, serialize=False, verbose_name="ID"
            ),
        ),
    ]
