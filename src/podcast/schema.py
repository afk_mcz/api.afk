import graphene
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.filter import DjangoFilterConnectionField

from .models import Episode, Podcast


class PodcastType(DjangoObjectType):
    image = graphene.String()

    class Meta:
        model = Podcast

    def resolve_image(self, args):
        return self.image and self.image.url


class EpisodeNode(DjangoObjectType):
    audio_mp3 = graphene.String()
    audio_ogg = graphene.String()
    image = graphene.String()
    duration = graphene.String()
    tracked_mp3 = graphene.String()
    full_text = graphene.String()

    class Meta:
        model = Episode
        filter_fields = ["publish", "podcast", "podcast__slug"]
        interfaces = (relay.Node,)

    def resolve_audio_mp3(self, args):
        return self.audio_mp3 and self.audio_mp3.url

    def resolve_audio_ogg(self, args):
        return self.audio_ogg and self.audio_ogg.url

    def resolve_image(self, args):
        return self.image and self.image.url

    def resolve_tracked_mp3(self, args):
        return self.tracked_mp3

    def resolve_full_text(self, args):
        return self.full_text


class Query(graphene.ObjectType):
    all_podcasts = graphene.List(PodcastType)
    all_episodes = DjangoFilterConnectionField(EpisodeNode)
    episode = relay.Node.Field(EpisodeNode, _id=graphene.Int(), slug=graphene.String())
    podcast = graphene.Field(PodcastType, _id=graphene.Int(), slug=graphene.String())

    def resolve_all_podcasts(self, info):
        return Podcast.objects.all()

    def resolve_episode(self, info, **kwargs):
        id = kwargs.get("id")
        slug = kwargs.get("slug")

        if id is not None:
            return Episode.objects.get(pk=id)

        if slug is not None:
            return Episode.objects.get(slug=slug)

        return None

    def resolve_podcast(self, info, **kwargs):
        id = kwargs.get("id")
        slug = kwargs.get("slug")

        if id is not None:
            return Podcast.objects.get(pk=id)

        if slug is not None:
            return Podcast.objects.get(slug=slug)

        return None
