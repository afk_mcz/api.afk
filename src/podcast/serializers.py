from rest_framework import serializers
from taggit.serializers import TagListSerializerField

from .models import Episode, Podcast, TaggedPodcast


class PodcastSerializer(serializers.ModelSerializer):
    tags = TagListSerializerField(required=False)
    feed = serializers.ReadOnlyField()

    class Meta:
        fields = "__all__"
        model = Podcast


class EpisodeSerializer(serializers.ModelSerializer):
    tracked_mp3 = serializers.ReadOnlyField()

    class Meta:
        model = Episode
        fields = (
            "publish",
            "order",
            "season",
            "episode_number",
            "episode_type",
            "podcast",
            "duration",
            "tracked_mp3",
            "audio_mp3",
            "audio_ogg",
            "audio_type",
            "audio_size",
            "title",
            "slug",
            "text",
            "small_text",
            "full_text",
            "full_text",
            "image",
            "dateCreated",
            "dateUpdated",
        )


class PodcastTagSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(many=False, read_only=True, slug_field="name")
    tag_id = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = TaggedPodcast
        fields = ("tag_id", "tag")
