from django.urls import include, path
from rest_framework import routers

from .feeds import AtomPodcastFeed, PodcastFeed
from .views import EpisodeViewSet, PodcastTagViewSet, PodcastViewSet

router = routers.DefaultRouter()
router.register(r"podcast", PodcastViewSet)
router.register(r"podcastTags", PodcastTagViewSet)
router.register(r"episodes", EpisodeViewSet)

urlpatterns = [
    path("rss/<slug:podcast_slug>", PodcastFeed(), name="podcast-feed-rss"),
    path("atom/<slug:podcast_slug>/", AtomPodcastFeed(), name="podcast-feed-atom"),
    path("json/", include(router.urls)),
]
