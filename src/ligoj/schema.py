import graphene
from apiArlefreak.custom_graphql_serializers import FlatTags
from graphene import relay
from graphene_django import DjangoObjectType
from graphene_django.converter import convert_django_field
from graphene_django.filter import DjangoFilterConnectionField
from taggit.managers import TaggableManager

from .models import Link


@convert_django_field.register(TaggableManager)
def convert_tag_field_to_string(field, registry=None):
    return graphene.Field(
        FlatTags, description=field.help_text, required=not field.null
    )


class LigoType(DjangoObjectType):
    class Meta:
        model = Link


class LigoNode(DjangoObjectType):
    class Meta:
        model = Link
        filter_fields = ["tags__name"]
        interfaces = (relay.Node,)


class Query(graphene.ObjectType):
    all_ligo = DjangoFilterConnectionField(LigoNode)

    ligo = relay.Node.Field(LigoNode, _id=graphene.Int())

    def resolve_ligo(self, info, **kwargs):
        id = kwargs.get("id")

        return Link.objects.get(pk=id)


schema = graphene.Schema(query=Query)
