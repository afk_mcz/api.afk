from django.contrib.syndication.views import Feed
from django.utils.feedgenerator import Atom1Feed, Rss201rev2Feed

from .models import LogFeed, Post


class ExtendedRSSFeed(Rss201rev2Feed):
    def rss_attributes(self):
        attrs = super(ExtendedRSSFeed, self).root_attributes()
        attrs["version"] = self._version
        attrs["xmlns:atom"] = "http://www.w3.org/2005/Atom"
        attrs["xmlns:content"] = "http://purl.org/rss/1.0/modules/content/"
        return attrs

    def add_root_elements(self, handler):
        super(ExtendedRSSFeed, self).add_root_elements(handler)
        handler.startElement(u"image", {})
        handler.addQuickElement(u"title", self.feed["title"])
        handler.addQuickElement(u"link", self.feed["link"])
        handler.addQuickElement(u"url", self.feed["image_url"])
        handler.endElement(u"image")

    def add_item_elements(self, handler, item):
        super(ExtendedRSSFeed, self).add_item_elements(handler, item)
        handler.addQuickElement(u"content:encoded", item["content_encoded"])


class LogsFeed(Feed):
    feed_type = ExtendedRSSFeed

    def feed_extra_kwargs(self, obj):
        extra_args = {"image_url": obj.image.url}

        return extra_args

    def item_extra_kwargs(self, item):
        return {"content_encoded": self.item_content_encoded(item)}

    def get_object(self, request, feed_slug):
        return LogFeed.objects.get(slug=feed_slug)

    def title(self, obj):
        return obj.title

    def link(self, obj):
        return obj.get_absolute_url()

    def description(self, obj):
        return obj.plain_text()

    # TODO: Override settings language
    def language(self, obj):
        return obj.language

    def author_name(self, obj):
        return obj.author

    def author_email(self, obj):
        return obj.author_mail

    def items(self, obj):
        if obj.excludeTags:
            excludeTags = obj.excludeTags.split(",")
            excludeTags = [x.strip(" ") for x in excludeTags]
            includeTags = None
            return (
                Post.objects.filter(publish=True)
                .exclude(tags__name__in=excludeTags)
                .distinct()
            )

        if obj.includeTags:
            includeTags = obj.includeTags.split(",")
            includeTags = [x.strip(" ") for x in includeTags]
            return Post.objects.filter(
                publish=True, tags__name__in=includeTags
            ).distinct()

    def subtitle(self, obj):
        return obj.small_text

    def item_pubdate(self, item):
        return item.dateCreated

    def item_updateddate(self, item):
        return item.dateUpdated

    def item_description(self, item):
        return item.plain_text

    def item_title(self, item):
        return item.title

    def item_content_encoded(self, item):
        return item.encoded_text()

    item_author_name = "[afk] Mario Carballo Zama"
    item_author_email = "afk@ellugar.co"


class AtomLogsFeed(LogsFeed):
    feed_type = Atom1Feed
    subtitle = LogsFeed.description
