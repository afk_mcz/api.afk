from django.urls import include, path
from rest_framework import routers

from . import views
from .feeds import AtomLogsFeed, LogsFeed

router = routers.DefaultRouter()
router.register(r"posts", views.PostViewSet)
router.register(r"postImages", views.PostImageViewSet)
router.register(r"postTags", views.PostTagViewSet)

urlpatterns = [
    path("", include(router.urls)),
    path("rss/<slug:feed_slug>", LogsFeed(), name="logs-feed-rss"),
    path("atom/<slug:feed_slug>/", AtomLogsFeed(), name="logs-feed-atom"),
]
