from datetime import datetime

import requests
from adminsortable.admin import SortableAdmin, SortableTabularInline
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Image, LogFeed, Post


def set_pepe_author(modeladmin, request, queryset):
    for item in queryset:
        item.author = "José Alberto Cardozo"
        item.author_url = "https://twitter.com/pepehxc"
        item.save()


def set_afk_author(modeladmin, request, queryset):
    for item in queryset:
        item.author = "Mario Carballo Zama"
        item.author_url = "https://twitter.com/afk_mario"
        item.save()


def update_publish_date(modeladmin, request, queryset):
    for item in queryset:
        item.dateCreated = datetime.now()
        item.save()


def send_update_signal(modeladmin, request, queryset):
    url = "https://api.netlify.com/build_hooks/5c91869f468f3ea7dfbab15e"
    requests.post(url)
    url = "https://api.netlify.com/build_hooks/5ec4bc52557f7aab50b28755"
    requests.post(url)


class AdminViewOnSiteMixin(object):
    @mark_safe
    def v_site(self, obj):
        return f"""<a
            class="button"
            target="_blank"
            style="white-space: nowrap;"
            href="{obj.get_absolute_url()}">
                View on site
        </a>
        """

    v_site.short_description = "Open on Site"


class AdminImageMixin(object):
    @mark_safe
    def admin_image(self, obj):
        try:
            return f"""
                    <div
                    style='height: 100px; width: 150px; display: block; background: whitesmoke'
                    >
                        <img
                        src='{obj.image.url}'
                        style='height: 100%; width: 100%; display: block; object-fit: cover;'
                        />
                    </div>
                    """
        except AttributeError:
            return "No Image"

    admin_image.short_description = "Preview"


class ImageInline(SortableTabularInline):
    model = Image
    extra = 0


@admin.register(LogFeed)
class LogFeedAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("order", "title", "slug", "dateCreated", "admin_image")
    list_display_links = ("order", "title", "slug", "dateCreated", "admin_image")


@admin.register(Post)
class PostAdmin(SortableAdmin, AdminImageMixin, AdminViewOnSiteMixin):
    date_hierarchy = "dateCreated"
    list_display = (
        "pk",
        "publish",
        "title",
        "dateCreated",
        "admin_image",
        "author",
        "author_url",
        "v_site",
    )
    list_display_links = ("title", "dateCreated", "admin_image", "author", "author_url")
    list_filter = ("publish", "author")
    list_editable = ("publish",)
    inlines = [
        ImageInline,
    ]
    actions = [update_publish_date, send_update_signal, set_afk_author, set_pepe_author]


@admin.register(Image)
class ImageAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("publish", "post", "altText", "caption", "admin_image")
    list_display_links = ("altText", "caption", "admin_image")
    list_editable = ("publish", "post")
    list_filter = ("post", "publish")
