from rest_framework import serializers

from .models import Entry, Image


class EntrySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = Entry


class ImageSerializer(serializers.ModelSerializer):
    thumbnail = serializers.ImageField(read_only=True)

    class Meta:
        fields = "__all__"
        model = Image
