from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"city", views.CityViewSet)
router.register(r"trip", views.TripViewSet)

urlpatterns = [path("", include(router.urls))]
