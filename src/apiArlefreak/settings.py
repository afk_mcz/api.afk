import os

import environ

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

root = environ.Path(__file__) - 3
env = environ.Env(DEBUG=(bool, False))
environ.Env.read_env()

SITE_ROOT = root()
SECRET_KEY = env("SECRET_KEY")
DEBUG = env("DEBUG")

ALLOWED_HOSTS = [
    "127.0.0.1",
    "0.0.0.0",
    "localhost",
    ".arlefreak.com",
    ".ellugar.co",
    "api.ellugar.co",
    "127.0.0.1",
    "localhost",
]

if DEBUG:
    ALLOWED_HOSTS = ["*"]

ADMINS = (("afk", "afk@ellugar.co"),)

if not DEBUG:
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    SECURE_BROWSER_XSS_FILTER = True
    SESSION_COOKIE_SECURE = True
    CSRF_COOKIE_SECURE = True

INSTALLED_APPS = [
    "storages",
    "adminsortable",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "corsheaders",
    "graphene_django",
    "imagekit",
    "rest_framework",
    "django_filters",
    "embed_video",
    "taggit",
    "solo",
    "location_field.apps.DefaultConfig",
    "colorful",
    "portfolio",
    "diary",
    "about",
    "ligoj",
    "podcast",
    "web_client",
    "cv",
    "nomad",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"
ROOT_URLCONF = "apiArlefreak.urls"

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [],
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ]
        },
    }
]

WSGI_APPLICATION = "apiArlefreak.wsgi.application"

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql_psycopg2",
        "NAME": env("DB_NAME"),
        "USER": env("DB_USER"),
        "PASSWORD": env("DB_PASSWORD"),
        "HOST": env("DB_HOST"),
        "PORT": env("DB_PORT"),
    }
}

# Password validation
# https://docs.djangoproject.com/en/1.9/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation."
        "UserAttributeSimilarityValidator"
    },
    {"NAME": "django.contrib.auth.password_validation." "MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation." "CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation." "NumericPasswordValidator"},
]

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = "es-mx"

TIME_ZONE = "UTC"

DATETIME_FORMAT = "%d-%m-%Y %H:%M:S"

USE_I18N = True

USE_L10N = True

USE_TZ = True

REST_FRAMEWORK = {
    "DEFAULT_FILTER_BACKENDS": ("django_filters.rest_framework.DjangoFilterBackend",),
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "rest_framework.authentication.BasicAuthentication",
        "rest_framework.authentication.SessionAuthentication",
        "rest_framework_simplejwt.authentication.JWTAuthentication",
    ),
}

CORS_ORIGIN_ALLOW_ALL = True
DO_SPACES_ENDPOINT = "nyc3.digitaloceanspaces.com"
BUCKET_NAME = env("AWS_STORAGE_BUCKET_NAME")

STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "apiArlefreak/static"),
]

if DEBUG:
    STATIC_URL = "/static/"
    STATICFILES_LOCATION = "static"
else:
    STATICFILES_LOCATION = "static"
    STATICFILES_STORAGE = "apiArlefreak.custom_storages.StaticStorage"
    STATIC_URL = f"https://{BUCKET_NAME}.{DO_SPACES_ENDPOINT}/{STATICFILES_LOCATION}/"

storages_options = {
    "access_key": env("AWS_ACCESS_KEY_ID"),
    "secret_key": env("AWS_SECRET_ACCESS_KEY"),
    "bucket_name": BUCKET_NAME,
    "endpoint_url": f"https://{DO_SPACES_ENDPOINT}",
    "querystring_auth": False,
    "default_acl": "public-read",
}

STORAGES = {
    "staticfiles": {
        "BACKEND": "storages.backends.s3.S3Storage",
        "OPTIONS": storages_options,
    },
    "default": {
        "BACKEND": "storages.backends.s3.S3Storage",
        "OPTIONS": storages_options,
    },
}


MEDIA_ROOT = "media"
MEDIAFILES_LOCATION = "media"

MEDIA_URL = f"https://{BUCKET_NAME}.{DO_SPACES_ENDPOINT}/{MEDIAFILES_LOCATION}/"

ADMIN_MEDIA_PREFIX = STATIC_URL + "admin/"
CKEDITOR_UPLOAD_PATH = "uploads/"
CKEDITOR_IMAGE_BACKEND = "pillow"

# Email
EMAIL_USE_TLS = True
EMAIL_HOST = env("EMAIL_HOST")
EMAIL_HOST_USER = env("EMAIL_HOST_USER")
EMAIL_HOST_PASSWORD = env("EMAIL_HOST_PASSWORD")
EMAIL_PORT = env("EMAIL_PORT")

GRAPHENE = {
    "SCHEMA": "apiArlefreak.schema.schema",
    "SCHEMA_OUTPUT": "apiArlefreak/static/data/schema.json",
    "DJANGO_CHOICE_FIELD_ENUM_V3_NAMING": True,
}
