from django.contrib import admin
from django.urls import include, path
from django.views.decorators.csrf import csrf_exempt
from graphene_django.views import GraphQLView
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

urlpatterns = [
    path("admin/", admin.site.urls),
    path("graphql", csrf_exempt(GraphQLView.as_view(graphiql=True))),
    path("token/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("token/verify/", TokenVerifyView.as_view(), name="token_verify"),
    path("portfolio/", include("portfolio.urls")),
    path("ligoj/", include("ligoj.urls")),
    path("diary/", include("diary.urls")),
    path("about/", include("about.urls")),
    path("web_client/", include("web_client.urls")),
    path("podcast/", include("podcast.urls")),
    path("cv/", include("cv.urls")),
    path("nomad/", include("nomad.urls")),
]
