from django.urls import include, path
from rest_framework import routers

from . import views

router = routers.DefaultRouter()
router.register(r"projects", views.ProjectViewSet)
router.register(r"projectsCategories", views.ProjectCategoryViewSet)
router.register(r"projectsImages", views.ProjectImageViewSet)
router.register(r"projectTags", views.ProjectTagViewSet)

urlpatterns = [path("", include(router.urls))]
