from rest_framework import serializers
from taggit.serializers import TagListSerializerField, TaggitSerializer


from .models import Image, Project, ProjectCategory, TaggedProject


class ProjectSerializer(TaggitSerializer, serializers.ModelSerializer):
    tags = TagListSerializerField(required=False)

    class Meta:
        fields = "__all__"
        model = Project


class ProjectCategorySerializer(serializers.ModelSerializer):
    class Meta:
        fields = "__all__"
        model = ProjectCategory


class ImageSerializer(serializers.ModelSerializer):
    thumbnail = serializers.ImageField(read_only=True)
    project = serializers.SlugRelatedField(read_only=True, slug_field="slug")

    class Meta:
        fields = "__all__"
        model = Image


class ProjectTagSerializer(serializers.ModelSerializer):
    tag = serializers.SlugRelatedField(many=False, read_only=True, slug_field="name")
    tag_id = serializers.PrimaryKeyRelatedField(many=False, read_only=True)

    class Meta:
        model = TaggedProject
        fields = ("tag_id", "tag")
