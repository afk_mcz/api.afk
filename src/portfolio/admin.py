from adminsortable.admin import SortableAdmin, SortableTabularInline
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Image, Project, ProjectCategory


class ImageInline(SortableTabularInline):
    model = Image
    extra = 0


class AdminImageMixin(object):
    @mark_safe
    def admin_image(self, obj):
        url = ""
        try:
            try:
                url = obj.image.url
            except AttributeError:
                url = obj.mainImage().url
            return f"""<img
                        src='{url}'
                        style='height: 100px; width: auto; display: block'
                />"""
        except Exception:
            return "None"

    admin_image.short_description = "Preview"


@admin.register(Project)
class ProjectAdmin(SortableAdmin, AdminImageMixin):
    date_hierarchy = "date"

    list_display = (
        "pk",
        "order",
        "publish",
        "name",
        "smallDescription",
        "category",
        "admin_image",
        "date",
    )
    list_display_links = ("name", "smallDescription", "admin_image", "date")
    list_filter = ("category", "publish")
    list_editable = ("publish", "category")

    inlines = [
        ImageInline,
    ]


@admin.register(ProjectCategory)
class ProjectCategoryAdmin(SortableAdmin, AdminImageMixin):
    list_display = ("pk", "order", "name", "smallDescription", "admin_image")
    list_display_links = ("name", "smallDescription", "admin_image")


@admin.register(Image)
class ImageAdmin(SortableAdmin, AdminImageMixin):
    list_display = (
        "pk",
        "order",
        "publish",
        "project",
        "name",
        "caption",
        "imgType",
        "admin_image",
    )
    list_display_links = ("name", "caption", "admin_image")
    list_editable = ("publish", "project", "imgType")
    list_filter = ("project", "publish")
