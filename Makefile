.PHONY: sort format lint

EXECUTABLES = isort black flake8
K := $(foreach exec,$(EXECUTABLES),\
        $(if $(shell which $(exec)),some string,$(error "No $(exec) in PATH")))

sort:
	@echo "[Sort] imports in src ..."
	@ isort ./src/**/*.py
	@echo " "

format:
	@echo "[Format] src ..."
	@ black ./src/**/*.py
	@echo " "

lint:
	@echo "[Lint] src ..."
	@ flake8 ./src/**/*.py
	@echo " "

run: sort format lint
	@echo "finished"
